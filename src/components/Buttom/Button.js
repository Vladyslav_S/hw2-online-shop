import React, { Component } from "react";
import PropTypes from "prop-types";

export default class Button extends Component {
  render() {
    const { text, onClick, className } = this.props;
    return (
      <>
        <button className={className} onClick={onClick}>
          {text}
        </button>
      </>
    );
  }
}

Button.propTypes = {
  text: PropTypes.string,
  onClick: PropTypes.func,
  className: PropTypes.string,
};
