import React, { Component } from "react";
import "./Card.scss";
import { star } from "../../themes/icons";
import Button from "../Buttom/Button";
import Modal from "../Modal/Modal";
import PropTypes from "prop-types";

class Card extends Component {
  state = {
    showModal: false,
    favorite: false,
  };

  componentDidMount() {
    if (localStorage.getItem(this.props.product.articul)) {
      this.setState({ favorite: true });
    }
  }

  addToCart(key) {
    if (localStorage.getItem("Cart" + key)) {
      // adding 1 more product to cart
      localStorage.setItem(
        "Cart" + key,
        +localStorage.getItem("Cart" + key) + 1
      );
    } else {
      localStorage.setItem("Cart" + key, 1);
    }
    this.closeModal();
  }

  addToFavorite = (key) => {
    if (localStorage.getItem(key, true)) {
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, true);
    }

    this.setState({
      favorite: !this.state.favorite,
    });
  };

  openModal = () => {
    this.setState({
      showModal: true,
    });
  };

  closeModal = () => {
    this.setState({ showModal: false });
  };

  render() {
    const { favorite, showModal } = this.state;
    const { product } = this.props;
    const { name, picPath, color, articul } = product;

    return (
      <div className="card-container" style={{ backgroundColor: color }}>
        <img src={picPath} alt="Album face" />
        <h4>{name}</h4>
        <p>Prise: 100500 $</p>
        <Button
          className="card-add-button"
          onClick={this.openModal}
          text={"Add to card"}
        />
        <span onClick={() => this.addToFavorite(articul)}>
          {star(favorite)}
        </span>
        {showModal && (
          <Modal
            text={`Are you sure you want to add "${name}" to cart?`}
            closeButton={true}
            closeButtonOnClick={this.closeModal}
            header="Adding to cart"
            actions={[
              <Button
                key={1}
                onClick={() => this.addToCart(articul)}
                className="modal-button"
                text="Ok"
              />,
              <Button
                key={2}
                onClick={this.closeModal}
                className="modal-button"
                text="Cancel"
              />,
            ]}
          />
        )}
      </div>
    );
  }
}

Card.propTypes = {
  product: PropTypes.shape({
    name: PropTypes.string,
    picPath: PropTypes.string,
    articul: PropTypes.string,
    color: PropTypes.string,
  }),
};

export default Card;
