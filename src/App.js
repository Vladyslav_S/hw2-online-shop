import "./App.scss";

import React, { Component } from "react";
import axios from "axios";
import Header from "./components/Header/Header";
import Card from "./components/Card/Card";

class App extends Component {
  state = {
    isLoading: true,
    products: [],
  };

  componentDidMount() {
    axios("./products.json").then((res) => {
      this.setState({ products: res.data });
      this.setState({ isLoading: false });
    });
  }

  render() {
    const { isLoading, products } = this.state;

    if (isLoading) {
      return <div className="App">Loading... Don`t go anywhere</div>;
    }

    return (
      <div className="App">
        <Header />

        <div className="container">
          {products.map((product) => {
            return <Card key={product.articul} product={product} />;
          })}
        </div>
      </div>
    );
  }
}

export default App;
